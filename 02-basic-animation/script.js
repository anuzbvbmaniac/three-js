// Cursor
const cursor = {
    x: 0,
    y: 0
}

window.addEventListener('mousemove', (_event) => {
    cursor.x = _event.clientX / sizes.width - 0.5
    cursor.y = -(_event.clientY / sizes.height - 0.5)
})

// Scene
const scene = new THREE.Scene()

// Red Cube
const cubeGeometry = new THREE.BoxGeometry(1, 1, 1, 5, 5, 5)
const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000 })
const cubeMesh = new THREE.Mesh(cubeGeometry, cubeMaterial)

scene.add(cubeMesh)

// Axes Helper
// const axesHelper = new THREE.AxesHelper(3)
// scene.add(axesHelper)


// Sizes
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight,
}

window.addEventListener('resize', () => {
    // Update Size
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Updaate Camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update Rendered
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

window.addEventListener("dblclick", () => {

    const fullScreenElement = document.fullscreenElement || document.webkitFullscreenElement

    if (!fullScreenElement) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen()
        } else if(canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen()
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen()
        } else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen()
        }
    }
})

const aspectRatio = sizes.width / sizes.height

// Cameras
const camera = new THREE.PerspectiveCamera(75, aspectRatio, 0.1, 100)
// const camera = new THREE.OrthographicCamera(-1 * aspectRatio, 1 * aspectRatio, 1, -1, 0.1, 100)
// camera.position.x = 2
// camera.position.y = 2
camera.position.z = 3
camera.lookAt(cubeMesh.position)
scene.add(camera)

// Renderer
const canvas = document.querySelector('.webgl')

const controls = new THREE.OrbitControls(camera, canvas)
controls.enableDamping = true

const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

// let time = Date.now()

// Clock
const clock = new THREE.Clock()

// Animate using GSAP
// gsap.to(cubeMesh.position, { x: 2, duration: 1, delay: 1 })
// gsap.to(cubeMesh.position, { x: 0, duration: 1, delay: 2 })


// Animation
const tick = () => {
    // Time: To Transform in all platform on same rate
    // const currentTime = Date.now()
    // const deltaTime = currentTime - time
    // time = currentTime

    const elapsedTime = clock.getElapsedTime()

    // Animate Cube
    // cubeAnimation(elapsedTime)

    // Animate Camera
    // cameraMovementAnimation(elapsedTime)

    // Update Camera
    // camera.position.x = Math.sin(cursor.x * Math.PI * 2) * 3
    // camera.position.z = Math.cos(cursor.x * Math.PI * 2) * 3
    // camera.position.y = cursor.y * 5
    // camera.lookAt(cubeMesh.position)

    // Update Controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    window.requestAnimationFrame(tick)
}

const cubeAnimation = (elapsedTime) => {
    // cubeMesh.position.x += 0.01
    // cubeMesh.position.y += 0.01
    // cubeMesh.rotation.y += 0.01
    cubeMesh.rotation.y = Math.sin(elapsedTime)
    // cubeMesh.rotation.x = Math.cos(elapsedTime)
}

const cameraMovementAnimation = (elapsedTime) => {
    camera.position.y = Math.sin(elapsedTime)
    camera.position.x = Math.cos(elapsedTime)
    camera.lookAt(cubeMesh.position)
}

tick()
