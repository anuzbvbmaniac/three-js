// Scene
const scene = new THREE.Scene()

// Group
const group = new THREE.Group()
group.position.y = 1
group.scale.y = 2
group.rotation.y = 1
scene.add(group)

const cube1 = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    new THREE.MeshBasicMaterial({ color: 0xFF0000 })
)

const cube2 = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    new THREE.MeshBasicMaterial({ color: 0x00FF00 })
)
cube2.position.x = -2

const cube3 = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    new THREE.MeshBasicMaterial({ color: 0x0000FF })
)
cube3.position.x = 2

group.add(cube1)
group.add(cube2)
group.add(cube3)

// Red Cube
// const cubeGeometry = new THREE.BoxGeometry(1, 1, 1)
// const cubeMaterial = new THREE.MeshBasicMaterial({ color: 0xff0000 })
// const cubeMesh = new THREE.Mesh(cubeGeometry, cubeMaterial)

// Position
// cubeMesh.position.x = 0.7
// cubeMesh.position.y = -0.6
// cubeMesh.position.z = 1
// cubeMesh.position.set(0.7, -0.6, 1)

// Scale
// cubeMesh.scale.x = 2
// cubeMesh.scale.y = 0.5
// cubeMesh.scale.z = 0.5
// cubeMesh.scale.set(2, 0.5, 0.5)

// Rotation
// cubeMesh.rotation.reorder('YXZ')
// cubeMesh.rotation.y = Math.PI // 3.14159
// cubeMesh.rotation.x = Math.PI * 0.25

// scene.add(cubeMesh)

// Axes Helper
const axesHelper = new THREE.AxesHelper(3)
scene.add(axesHelper)


// Sizes
const sizes = {
    width: 800,
    height: 600,
}


// Camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height)
camera.position.z = 3
scene.add(camera)


// Renderer
const canvas = document.querySelector('.webgl')
const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
})
renderer.setSize(sizes.width, sizes.height)
renderer.render(scene, camera)
